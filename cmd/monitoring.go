package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	monitoring "cloud.google.com/go/monitoring/apiv3/v2"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/golang/protobuf/ptypes/timestamp"
	"go.uber.org/zap"
	"google.golang.org/genproto/googleapis/api/label"
	"google.golang.org/genproto/googleapis/api/metric"
	metricpb "google.golang.org/genproto/googleapis/api/metric"
	"google.golang.org/genproto/googleapis/api/monitoredres"
	monitoringpb "google.golang.org/genproto/googleapis/monitoring/v3"
)

var environment string
var instanceID int
var zone string

func main() {
	logger, _ := zap.NewProduction()
	zap.ReplaceGlobals(logger)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)

	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	zap.S().Info("created client")

	environment, err = fetchEnvironment()
	if err != nil {
		panic(err)
	}

	zap.S().Infof("fetched env: %s", environment)

	instanceID, err = fetchInstanceID()
	if err != nil {
		panic(err)
	}

	zap.S().Infof("fetched instance ID: %d", instanceID)

	zone, err = fetchZone()
	if err != nil {
		panic(err)
	}

	zap.S().Infof("fetched zone: %s", zone)

	_, err = createMetric()
	if err != nil {
		panic(err)
	}

	zap.S().Info("created metric")

	ticker := time.NewTicker(15 * time.Second)
	done := make(chan bool)

	zap.S().Info("started ticker")

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				zap.S().Info("ticker ticked")
				containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
				if err != nil {
					zap.S().Errorf("could not get containers: %v", err)
				}

				zap.S().Infof("got %d containers", len(containers))
				err = writeMetricValue(int64(len(containers)))
				if err != nil {
					zap.S().Errorf("could not write metric: %v", err)
				}
			}
		}
	}()

	resp, err := cli.ContainerCreate(
		context.Background(),
		&container.Config{
			Image: "registry.gitlab.com/codecollab-io/compiler/cc-compiler-servant:latest",
		},
		&container.HostConfig{
			PortBindings: map[nat.Port][]nat.PortBinding{
				"8111/tcp": {{
					HostIP:   "0.0.0.0",
					HostPort: "8111",
				}},
			},
			Binds: []string{
				"/var/run/docker.sock:/var/run/docker.sock",
				"/var/codecollab/cc-compiler-servant:/node/bin/env",
			},
		}, nil, nil, "servant")
	if err != nil {
		panic(err)
	}

	zap.S().Info("created servant")

	err = cli.ContainerStart(context.Background(), resp.ID, types.ContainerStartOptions{})
	if err != nil {
		panic(err)
	}

	zap.S().Info("started servant")

	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		// Explicit;y ignore error because it should not fail.
		// If it fails, the instance will be recreated
		_, _ = w.Write([]byte("ok"))
	})

	go func() {
		err = http.ListenAndServe(":5000", nil)
		if err != nil {
			zap.S().Fatal(err)
		}
	}()

	<-interrupt

	ticker.Stop()
	done <- true
}

func createMetric() (*metricpb.MetricDescriptor, error) {
	ctx := context.Background()
	c, err := monitoring.NewMetricClient(ctx)
	if err != nil {
		return nil, fmt.Errorf("could not create metric client: %w", err)
	}

	defer func(c *monitoring.MetricClient) {
		_ = c.Close()
	}(c)

	md := &metric.MetricDescriptor{
		Name:        "Slave count",
		Description: "Count of slaves running on the servant",
		Type:        "custom.googleapis.com/instance/slaves/count",
		Labels:      []*label.LabelDescriptor{},
		MetricKind:  metric.MetricDescriptor_GAUGE,
		ValueType:   metric.MetricDescriptor_INT64,
		Unit:        "1",
		DisplayName: "Slave count",
	}

	req := &monitoringpb.CreateMetricDescriptorRequest{
		Name:             "projects/codecollab-io",
		MetricDescriptor: md,
	}

	m, err := c.CreateMetricDescriptor(ctx, req)
	if err != nil {
		return nil, fmt.Errorf("could not create custom metric: %w", err)
	}

	return m, nil
}

func writeMetricValue(count int64) error {
	ctx := context.Background()
	c, err := monitoring.NewMetricClient(ctx)
	if err != nil {
		return err
	}

	defer func(c *monitoring.MetricClient) {
		_ = c.Close()
	}(c)

	now := &timestamp.Timestamp{
		Seconds: time.Now().Unix(),
	}

	req := &monitoringpb.CreateTimeSeriesRequest{
		Name: "projects/codecollab-io",
		TimeSeries: []*monitoringpb.TimeSeries{
			{
				Metric: &metricpb.Metric{
					Type: "custom.googleapis.com/instance/slaves/count",
					Labels: map[string]string{
						"env": environment,
					},
				},
				Resource: &monitoredres.MonitoredResource{
					Type: "gce_instance",
					Labels: map[string]string{
						"project_id":  "codecollab-io",
						"instance_id": strconv.Itoa(instanceID),
						"zone":        zone,
					},
				},
				Points: []*monitoringpb.Point{
					{
						Interval: &monitoringpb.TimeInterval{
							StartTime: now,
							EndTime:   now,
						},
						Value: &monitoringpb.TypedValue{
							Value: &monitoringpb.TypedValue_Int64Value{Int64Value: count},
						},
					},
				},
			},
		},
	}

	zap.S().Infof("write time series request: %+v\n", req)

	err = c.CreateTimeSeries(ctx, req)
	if err != nil {
		return fmt.Errorf("could not write time series value: %w", err)
	}

	return nil
}

func fetchInstanceID() (int, error) {
	c := http.Client{Timeout: 10 * time.Second}
	req, err := http.NewRequest("GET", "http://metadata.google.internal/computeMetadata/v1/instance/id", nil)
	if err != nil {
		return 0, err
	}

	req.Header = http.Header{
		"Metadata-Flavor": []string{"Google"},
	}

	res, err := c.Do(req)
	if err != nil {
		return 0, err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(res.Body)

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return 0, err
	}

	return strconv.Atoi(string(body))
}

func fetchEnvironment() (string, error) {
	c := http.Client{Timeout: 10 * time.Second}
	req, err := http.NewRequest("GET", "http://metadata.google.internal/computeMetadata/v1/instance/attributes/env", nil)
	if err != nil {
		return "", err
	}

	req.Header = http.Header{
		"Metadata-Flavor": []string{"Google"},
	}

	res, err := c.Do(req)
	if err != nil {
		return "", err
	}

	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(res.Body)

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func fetchZone() (string, error) {
	c := http.Client{Timeout: 10 * time.Second}
	req, err := http.NewRequest("GET", "http://metadata.google.internal/computeMetadata/v1/instance/zone", nil)
	if err != nil {
		return "", err
	}

	req.Header = http.Header{
		"Metadata-Flavor": []string{"Google"},
	}

	res, err := c.Do(req)
	if err != nil {
		return "", err
	}

	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(res.Body)

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	z := string(body)
	part := strings.Split(z, "/")

	return part[len(part)-1], nil
}
