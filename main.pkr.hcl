packer {
  required_plugins {
    googlecompute = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/googlecompute"
    }
  }
}

variable "REGION" {
  type = string
}

variable "CI_REGISTRY" {
  type = string
}

variable "CI_REGISTRY_USER" {
  type = string
}

variable "CI_REGISTRY_PASSWORD" {
  type = string
}

variable "CI_SERVICE_ACCOUNT" {
  type = string
}

variable "TAG" {
  type = string
}

variable "CI_COMMIT_SHORT_SHA" {
  type = string
}

variable "CC_COMPILER_SLAVE_IMAGE" {
  type = string
}

variable "CC_COMPILER_SERVANT_IMAGE" {
  type = string
}

variable "CC_COMPILER_SERVANT_PACKER_IMAGE" {
  type = string
}

variable "CC_COMPILER_SERVANT_SECRET_ID" {
  type = string
}

source "googlecompute" "cc_compiler_servant" {
  project_id = "codecollab-io"
  zone       = "us-central1-a"

  # Image configuration
  source_image_family = "cos-89-lts"
  image_name          = format("cc-compiler-servant-%s", uuidv4())
  image_family        = format("cc-compiler-servant-%s", var.REGION)
  image_description   = format("Created by Packer from cc-compiler-servant-packer. SHA: %s", var.CI_COMMIT_SHORT_SHA)

  # Machine configuration
  # - must use preemptible so we don't go broke running build jobs
  preemptible  = true
  machine_type = "n1-standard-1"
  ssh_username = "cc-compiler-servant-packer"
  scopes       = [
    "https://www.googleapis.com/auth/cloud-platform"
  ]

  service_account_email = var.CI_SERVICE_ACCOUNT

  disk_size = 20
  disk_type = "pd-standard"

  enable_secure_boot          = true
  enable_vtpm                 = true
  enable_integrity_monitoring = true

  instance_name = format("cc-compiler-servant-%s", uuidv4())
}

build {
  sources = [
    "sources.googlecompute.cc_compiler_servant"
  ]

  provisioner "shell" {
    inline = [
      format("docker login -u %s -p %s %s", var.CI_REGISTRY_USER, var.CI_REGISTRY_PASSWORD, var.CI_REGISTRY),

      format("docker pull %s:%s", var.CC_COMPILER_SLAVE_IMAGE, var.TAG),
      format("docker pull %s:%s", var.CC_COMPILER_SERVANT_IMAGE, var.TAG),
      format("docker pull %s:%s", var.CC_COMPILER_SERVANT_PACKER_IMAGE, var.TAG),
      format("docker tag %s:%s %s:latest", var.CC_COMPILER_SLAVE_IMAGE, var.TAG, var.CC_COMPILER_SLAVE_IMAGE),
      format("docker tag %s:%s %s:latest", var.CC_COMPILER_SERVANT_IMAGE, var.TAG, var.CC_COMPILER_SERVANT_IMAGE),
      format("docker tag %s:%s %s:latest", var.CC_COMPILER_SERVANT_PACKER_IMAGE, var.TAG, var.CC_COMPILER_SERVANT_PACKER_IMAGE),
      "toolbox",

      "sudo mkdir /var/codecollab",
      "sudo chmod 777 /var/codecollab",
      "mkdir /var/codecollab/cc-compiler-servant",

      format("toolbox /google-cloud-sdk/bin/gcloud secrets versions access latest --secret %s > /var/codecollab/cc-compiler-servant/.env", var.CC_COMPILER_SERVANT_SECRET_ID),
      "docker rmi $(docker images 'gcr.io/google-containers/toolbox' -q)"
    ]

    execute_command = "chmod +x {{ .Path }}; {{ .Vars }} bash {{ .Path }}"
  }
}
