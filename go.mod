module gitlab.com/codecollab-io/compiler/cc-compiler-servant-packer

go 1.16

require (
	cloud.google.com/go/monitoring v0.1.0
	github.com/containerd/containerd v1.5.5 // indirect
	github.com/docker/docker v20.10.8+incompatible
	github.com/docker/go-connections v0.4.0
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/moby/term v0.0.0-20210619224110-3f7ff695adc6 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	go.uber.org/zap v1.10.0
	google.golang.org/genproto v0.0.0-20210909211513-a8c4777a87af
)
